package plugins;

import interfacesModelo.Notificador;
import interfacesModelo.Observable;
import modelo.Validador;

public class ImpNotificador1 implements Notificador{

	String nombre = "mail";

	@Override
	public String getNombre() {
		return nombre;
	}
	
	@Override
	public String toString() {
		return nombre;
	}
	
	@Override
	public void actualizar(Observable obs) {
		Validador v = (Validador) obs;
		System.out.println("-------------------------------\n"
				+ "Se envio un mail con los siguientes datos: \n"+ "Agredido: " + v.getPerimetral().getAgredido().getNombre() + "\nAgresor: " + v.getPerimetral().getAgresor().getNombre() + "\nEstado:" + v.getEstadoPerimetral());	
	}
	
}
