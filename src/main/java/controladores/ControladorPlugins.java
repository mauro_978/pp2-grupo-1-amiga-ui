package controladores;

import java.awt.event.ActionEvent;
import PP2.Grupo.AMIGA.UI.vistas.UIPlugins;
import configuracion.SistemaAmiga;
import interfacesModelo.Notificador;

public class ControladorPlugins {
	
	private SistemaAmiga modelo;
	private UIPlugins uiPlugins;
	
	public ControladorPlugins(SistemaAmiga modelo, UIPlugins uiPlugins) {
		this.modelo = modelo;
		this.uiPlugins = uiPlugins;
		
		uiPlugins.getBtnHabilitar().addActionListener(habPlug -> habilitarPlugin(habPlug));
		uiPlugins.getBtnDeshabilitar().addActionListener(deshPlug -> deshabilitarPlugin(deshPlug));
	}
	
	public void inicializar() {
		refrescarTablaPlugins();
		this.uiPlugins.mostrar();
	}
	
	
	private void refrescarTablaPlugins() {
		this.uiPlugins.llenarTabla(this.modelo.getNotificadores());
	}

	private void habilitarPlugin(ActionEvent habPlug){
		int filaSeleccionada = uiPlugins.getTabla().getSelectedRow();
		Notificador n = (Notificador)uiPlugins.getTabla().getValueAt(filaSeleccionada, 0);
		modelo.habilitarDeshabilitarNotificador(n, true);
		refrescarTablaPlugins();
	}
	
	private void deshabilitarPlugin(ActionEvent deshPlug){
		int filaSeleccionada = uiPlugins.getTabla().getSelectedRow();
		Notificador n = (Notificador)uiPlugins.getTabla().getValueAt(filaSeleccionada, 0);
		modelo.habilitarDeshabilitarNotificador(n, false);
		refrescarTablaPlugins();
	}
	
	
}
