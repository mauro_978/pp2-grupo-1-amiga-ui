package controladores;

import java.awt.event.ActionEvent;
import PP2.Grupo.AMIGA.UI.vistas.UIPrincipal;

public class ControladorUIPrincipal {

	private UIPrincipal uiPrincipal;
	private ControladorUIActualizarUbicaciones cUIAU;
	private ControladorPlugins cp;
	
	public ControladorUIPrincipal(UIPrincipal uiPrincipal, ControladorUIActualizarUbicaciones cUIAU, ControladorPlugins cp) {
		this.uiPrincipal = uiPrincipal;
		this.cUIAU = cUIAU;
		this.cp = cp;
		
		uiPrincipal.getMIActualizarUbicacionDispositivo().addActionListener(mUIAU -> mostrarUIActualizarUbicaciones(mUIAU));
		uiPrincipal.getMIPlugins().addActionListener(mUIP -> mostrarUIPlugins(mUIP));
	}
	
	public void inicializar() {
		this.uiPrincipal.mostrar();
	}

	private void mostrarUIActualizarUbicaciones(ActionEvent mUIAU){
		this.cUIAU.inicializar();
	}
	
	private void mostrarUIPlugins(ActionEvent mUIP){
		this.cp.inicializar();
	}
}
