package controladores;

import java.awt.event.ActionEvent;

import PP2.Grupo.AMIGA.UI.vistas.UIActualizarUbicaciones;
import configuracion.SistemaAmiga;
import modelo.Dispositivo;
import modelo.Ubicacion;

public class ControladorUIActualizarUbicaciones {
		
		private SistemaAmiga modelo;
		private UIActualizarUbicaciones uiActualizarUbicacion;
		
		public ControladorUIActualizarUbicaciones(SistemaAmiga modelo, UIActualizarUbicaciones uiActualizarUbicacion) {
			this.modelo = modelo;
//			this.uiActualizarUbicacion = UIActualizarUbicaciones.getInstance();
			this.uiActualizarUbicacion = uiActualizarUbicacion;
			
			uiActualizarUbicacion.getBtnActualizarUbicacion().addActionListener(acUD -> actualizarUbicacionDispositivo(acUD));
		}
		
		public void inicializar() {
			this.uiActualizarUbicacion.mostrar();
		}

		private void actualizarUbicacionDispositivo(ActionEvent acUD){
			//si uso el idDispositivo tengo dos opciones o pido los validadores los recorro 
			//hasta q encuentro un dispositivo que tenga este idDispositivo y lo modifico
			//o pongo un metodo en modelo q me devuelva un dispositivo a partir de su id
			
			//otra opcion seria tener una lista de personas en la interfaz y q cuando actualizo reviso la seleccionada y 
			//busco su dispositivo y lo actualizo
			
			String idDispositivo = this.uiActualizarUbicacion.getTxtIdDispositivo().getText();
			double latitud = Double.parseDouble(uiActualizarUbicacion.getTxtLatitud().getText());
			double longitud = Double.parseDouble(uiActualizarUbicacion.getTxtLongitud().getText());
			
			Dispositivo d = this.modelo.getDispositivo(idDispositivo);
			Ubicacion nuevaUbicacion = new Ubicacion(latitud, longitud);
			d.actualizarUbicacion(nuevaUbicacion);
		}
		

}
