package PP2.Grupo.AMIGA.UI.main;


import PP2.Grupo.AMIGA.UI.vistas.UIActualizarUbicaciones;
import PP2.Grupo.AMIGA.UI.vistas.UIMapa;
import PP2.Grupo.AMIGA.UI.vistas.UIPerimetrales;
import PP2.Grupo.AMIGA.UI.vistas.UIPlugins;
import PP2.Grupo.AMIGA.UI.vistas.UIPrincipal;
import configuracion.SistemaAmiga;
import controladores.ControladorPlugins;
import controladores.ControladorUIActualizarUbicaciones;
import controladores.ControladorUIPrincipal;

public class Main {

	public static void main(String[] args) {
		SistemaAmiga model = new SistemaAmiga();
		model.inicializar();
		
		UIPlugins uiPlugins = new UIPlugins();
		ControladorPlugins cplugins = new ControladorPlugins(model, uiPlugins);
		
		UIActualizarUbicaciones uiActUbi = new UIActualizarUbicaciones();
		ControladorUIActualizarUbicaciones cau = new ControladorUIActualizarUbicaciones(model, uiActUbi);
		
		UIPerimetrales uiperi = new UIPerimetrales(model);
		uiperi.mostrar();
		
		UIMapa vp = new UIMapa(model);
		
		UIPrincipal uip = new UIPrincipal(uiperi, vp);
		ControladorUIPrincipal cUIP = new ControladorUIPrincipal(uip, cau, cplugins);
		cUIP.inicializar();
		
		
		
	}
}
