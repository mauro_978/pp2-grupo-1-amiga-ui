package PP2.Grupo.AMIGA.UI.vistas;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import configuracion.SistemaAmiga;
import interfacesModelo.Observable;
import interfacesModelo.Observer;
import modelo.Ubicacion;
import modelo.Validador;

public class UIPerimetrales extends JScrollPane implements Observer {
	
	private static final long serialVersionUID = 1L;
	private JTable table;
	private SistemaAmiga modelo;
	private DefaultTableModel modelPerimetrales;
	private String[] nombreColumnas = {"Agresor", "IdDispositivo", "Ubicacion", "Agredido", "IdDispositivo", "Ubicacion", "Estado"};
	
	public UIPerimetrales(SistemaAmiga modelo){
		
		super();
		this.modelo = modelo;
		initialize();
		modelo.attachObserverDispsitivos(this);
		modelo.attachObserverValidadores(this);
	}
	
	public void initialize() {
		
		setPreferredSize(new Dimension(100,100));
		
		modelPerimetrales = new DefaultTableModel(null,nombreColumnas);
		table = new JTable(modelPerimetrales);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getColumnModel().getColumn(0).setResizable(false);
		setViewportView(table);	
		
		table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
			private static final long serialVersionUID = 1L;

			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

			String estado = (String) table.getValueAt(row, 6);
			
			if(estado.equals("SEGURO")) {
				setBackground(new Color(209,  255, 144));
		    }else if(estado.equals("PRECAUCION")) {
		    	setBackground(new Color(255, 220, 154));
		    }else if(estado.equals("PELIGRO")) {
		    	setBackground(new Color(255,  156, 154));
		    }else if(estado.equals("DESCONOCIDO")) {
		    	setBackground(new Color(211,  255, 241));
		    }
			
			return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}
			});
	}
	
	public void mostrar() {
		dibujar();
	}
	
	public void dibujar() {
		this.modelPerimetrales.setRowCount(0); //Para vaciar la tabla
		this.modelPerimetrales.setColumnCount(0);
		this.modelPerimetrales.setColumnIdentifiers(this.nombreColumnas);
		
		for (Validador v : modelo.getValidadores()){
			String agresor = v.getPerimetral().getAgresor().getNombre();
			String idDispAgresor = modelo.getDispositivo(v.getPerimetral().getAgresor()).getIdDispositivo();
			Ubicacion uagresor = modelo.getDispositivo(v.getPerimetral().getAgresor()).getUbicacionDispositivo();
			String ubAgresor = "lat: " + uagresor.getLatitud() + " long: " + uagresor.getLongitud();
			
			String agredido = v.getPerimetral().getAgredido().getNombre();
			String idDispAgredido = modelo.getDispositivo(v.getPerimetral().getAgredido()).getIdDispositivo();
			Ubicacion uagredido = modelo.getDispositivo(v.getPerimetral().getAgredido()).getUbicacionDispositivo();
			String ubAgredido = "lat: " + uagredido.getLatitud() + " long: " + uagredido.getLongitud();
			
			Object[] fila = {agresor, idDispAgresor, ubAgresor, agredido, idDispAgredido, ubAgredido, v.getEstadoPerimetral()};
			this.modelPerimetrales.addRow(fila);
		}
	}
	
	@Override
	public void actualizar(Observable obs) {
		dibujar();
	}

}
