package PP2.Grupo.AMIGA.UI.vistas;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import org.jxmapviewer.JXMapKit;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.painter.CompoundPainter;
import org.jxmapviewer.painter.Painter;
import org.jxmapviewer.viewer.DefaultWaypoint;
import org.jxmapviewer.viewer.GeoPosition;
import org.jxmapviewer.viewer.Waypoint;
import org.jxmapviewer.viewer.WaypointPainter;

import configuracion.SistemaAmiga;
import interfacesModelo.Observable;
import interfacesModelo.Observer;
import modelo.Validador;

public class UIMapa extends JXMapKit implements Observer{

	private static final long serialVersionUID = 1L;
	private SistemaAmiga modelo;
	
	
	public UIMapa(SistemaAmiga modelo){
		
		super();
		this.modelo = modelo;
		initialize();
		modelo.attachObserverDispsitivos(this);
		modelo.attachObserverValidadores(this);
	}
	
	
	
	public void initialize() {
		
		setVisible(true);
		setMiniMapVisible(false);
	 	setZoom(3);
	 	setCenterPosition(new GeoPosition(-34.55, -58.705));

	}
	
	
	public void mostrar() {
		dibujar();
	}
	public void dibujar() {
		
		ArrayList<GeoPosition> tr = new ArrayList<GeoPosition>();
		Set<Waypoint> way = new HashSet<Waypoint>();
		List<Painter<JXMapViewer>> pain = new ArrayList<Painter<JXMapViewer>>();
		
		for(Validador v : modelo.getValidadores()) {
			GeoPosition UAgresor = new GeoPosition(modelo.getDispositivo(v.getPerimetral().getAgresor()).getUbicacionDispositivo().getLatitud(), modelo.getDispositivo(v.getPerimetral().getAgresor()).getUbicacionDispositivo().getLongitud());
			GeoPosition UAgredido = new GeoPosition(modelo.getDispositivo(v.getPerimetral().getAgredido()).getUbicacionDispositivo().getLatitud(), modelo.getDispositivo(v.getPerimetral().getAgredido()).getUbicacionDispositivo().getLongitud());
//			List<GeoPosition> track = Arrays.asList(UAgresor, UAgredido);
			tr.add(UAgredido);
			tr.add(UAgresor);
//			Set<Waypoint> waypoints = new HashSet<Waypoint>(Arrays.asList(
//	                new DefaultWaypoint(UAgresor),
//	                new DefaultWaypoint(UAgredido)));
			way.add(new DefaultWaypoint(UAgresor));
			way.add(new DefaultWaypoint(UAgredido));
			
			WaypointPainter<Waypoint> waypointPainter = new WaypointPainter<Waypoint>();
//	        waypointPainter.setWaypoints(waypoints);
			waypointPainter.setWaypoints(way);
//	        List<Painter<JXMapViewer>> painters = new ArrayList<Painter<JXMapViewer>>();
	//      painters.add(routePainter);
//	        painters.add(waypointPainter);
			pain.add(waypointPainter);
//	        CompoundPainter<JXMapViewer> painter = new CompoundPainter<JXMapViewer>(painters);
//			getMainMap().setOverlayPainter(painter);
//			break;
		}
		CompoundPainter<JXMapViewer> painter = new CompoundPainter<JXMapViewer>(pain);
		getMainMap().setOverlayPainter(painter);
	}

	@Override
	public void actualizar(Observable obs) {
		dibujar();
	}
}
