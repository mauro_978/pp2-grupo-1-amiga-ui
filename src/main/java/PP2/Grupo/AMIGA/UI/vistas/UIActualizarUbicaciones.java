package PP2.Grupo.AMIGA.UI.vistas;



import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class UIActualizarUbicaciones{
	

//	private static UIActualizarUbicaciones INSTANCE;
	private JFrame frame;
	private JTextField idDispositivo;
	private JTextField longitud;
	private JTextField latitud;
	private JButton actualizarUbicacion;
	
	
//	public static UIActualizarUbicaciones getInstance(){
//		if(INSTANCE == null){
//			INSTANCE = new UIActualizarUbicaciones(); 	
//			return INSTANCE;
//		}else return INSTANCE;
//	}

	
	public UIActualizarUbicaciones(){
		super();
		initialize();
	}
	
	public void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 400, 270);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 399, 269);
		frame.add(panel);
		panel.setLayout(null);
		
		JLabel lblIdDispositivo = new JLabel();
		lblIdDispositivo.setBounds(30, 30, 150, 30);
		lblIdDispositivo.setText("Id del dispositivo: ");
		panel.add(lblIdDispositivo);
		
		idDispositivo = new JTextField();
		idDispositivo.setBounds(190, 30, 150, 30);
		panel.add(idDispositivo);
		
		JLabel lblLongitud = new JLabel();
		lblLongitud.setBounds(30, 70, 150, 30);
		lblLongitud.setText("Longitud: ");
		panel.add(lblLongitud);
		
		longitud = new JTextField();
		longitud.setBounds(190, 70, 150, 30);
		panel.add(longitud);
		
		JLabel lblLatitud = new JLabel();
		lblLatitud.setBounds(30, 110, 150, 30);
		lblLatitud.setText("Latitud: ");
		panel.add(lblLatitud);
		
		latitud = new JTextField();
		latitud.setBounds(190, 110, 150, 30);
		panel.add(latitud);
		
		actualizarUbicacion = new JButton();
		actualizarUbicacion.setBounds(190, 150, 150, 30);
		actualizarUbicacion.setText("Actualizar Ubicación");
		panel.add(actualizarUbicacion);
	}
	
	public void mostrar() {
		this.frame.setVisible(true);
	}
	
	public void cerrar() {
		this.frame.setVisible(false);
	}

	public JTextField getTxtIdDispositivo() {
		return idDispositivo;
	}


	public JTextField getTxtLongitud() {
		return longitud;
	}


	public JTextField getTxtLatitud() {
		return latitud;
	}

	public JButton getBtnActualizarUbicacion() {
		return actualizarUbicacion;
	}
	
}
