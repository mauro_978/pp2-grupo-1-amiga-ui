package PP2.Grupo.AMIGA.UI.vistas;

import java.util.Map;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import interfacesModelo.Notificador;

public class UIPlugins {
	private JFrame frame;
	private JTable table;
	private DefaultTableModel modelPlugins;
	private String[] nombreColumnas = {"Plugin", "Estado"};
	private JButton btnHabilitar;
	private JButton btnDeshabilitar;
	
	public UIPlugins() {
		super();
		initialize();
	}
	
	
	public void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 1069, 300);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1059, 262);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(10, 13, 917, 182);
		panel.add(spPersonas);
		
		modelPlugins = new DefaultTableModel(null,nombreColumnas);
		table = new JTable(modelPlugins);
		
		spPersonas.setViewportView(table);	
		
		btnHabilitar = new JButton();
		btnDeshabilitar = new JButton();
		btnHabilitar.setBounds(940, 12, 110, 30);
		btnDeshabilitar.setBounds(940, 52, 110, 30);
		btnHabilitar.setText("Habilitar");
		btnDeshabilitar.setText("Deshabilitar");
		panel.add(btnDeshabilitar);
		panel.add(btnHabilitar);
		
	}
	
	public void mostrar() {
		this.frame.setVisible(true);
	}
	
	public void cerrar() {
		this.frame.setVisible(false);
	}
	
	public void llenarTabla(Map<Notificador, Boolean> notificadores) {
		this.modelPlugins.setRowCount(0); //Para vaciar la tabla
		this.modelPlugins.setColumnCount(0);
		this.modelPlugins.setColumnIdentifiers(this.nombreColumnas);
		
		for (Map.Entry<Notificador, Boolean> entry : notificadores.entrySet()) {
			Notificador notificador = entry.getKey();
			String estado;
			if(entry.getValue()) estado = "Habilitado";
			else estado = "Deshabilitado";
			
			Object[] fila = {notificador, estado};
			this.modelPlugins.addRow(fila);
		}
	}
	
	public JTable getTabla(){
		return table;
	}

	
	public JButton getBtnHabilitar() {
		return btnHabilitar;
	}
	
	public JButton getBtnDeshabilitar() {
		return btnDeshabilitar;
	}
	
}
