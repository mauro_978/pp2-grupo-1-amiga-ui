package PP2.Grupo.AMIGA.UI.vistas;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class UIPrincipal {
	
	private JFrame frame;
	private JMenuBar menuBar;
    private JMenu menuConfigurar;
    private JMenuItem miActUbicacionDispositivo, miPlugins;

	public UIPrincipal(UIPerimetrales uiPerimetrales, UIMapa mapa){
		super();
		initialize(uiPerimetrales, mapa);
	}
	
	public void initialize(UIPerimetrales uiPerimetrales, UIMapa mapa) {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 1069, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		menuBar=new JMenuBar();
		frame.setJMenuBar(menuBar);
        menuConfigurar=new JMenu("Configurar");
        menuBar.add(menuConfigurar);
        miActUbicacionDispositivo=new JMenuItem("Ubicaciones Dispositivos");
        menuConfigurar.add(miActUbicacionDispositivo);
        miPlugins=new JMenuItem("Plugins");
        menuConfigurar.add(miPlugins);
		
		frame.add(mapa, BorderLayout.CENTER);
		frame.add(uiPerimetrales, BorderLayout.SOUTH);
	}
	
	public void mostrar() {
		this.frame.setVisible(true);
	}
	
	public JMenuItem getMIActualizarUbicacionDispositivo() {
		return miActUbicacionDispositivo;
	}

	public JMenuItem getMIPlugins() {
		return miPlugins;
	}
}
